import Main from './components/Main'
import { useSelector } from 'react-redux'

const App = () => {
    return <Main state={useSelector((state) => state)} />
}

export default App
