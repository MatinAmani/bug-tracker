import { removeBug, resolveBug } from '../redux/actions/BugActions'
import { useDispatch } from 'react-redux'
import { Paper, Divider } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'

import AddBugForm from '../components/Sections/AddBug'
import List from '../components/Sections/List'

const useStyles = makeStyles((theme) => ({
    paper: {
        width: '40vw',
        [theme.breakpoints.down('sm')]: {
            width: '75vw',
        },
    },
    formContainer: {
        padding: theme.spacing(2),
    },
}))

const Main = ({ state }) => {
    const classes = useStyles()
    const dispatch = useDispatch()

    const handleResolve = (id) => dispatch(resolveBug(id))

    const handleDelete = (id) => dispatch(removeBug(id))

    return (
        <Paper className={classes.paper} elevation={3}>
            <div className={classes.formContainer}>
                <AddBugForm />
            </div>
            <Divider />
            <div className="list">
                <List
                    bugs={state}
                    onDelete={handleDelete}
                    onResolve={handleResolve}
                />
            </div>
        </Paper>
    )
}

export default Main
