import { useFormik } from 'formik'
import { useDispatch } from 'react-redux'
import { TextField, Button } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'
import { addBug } from '../../redux/actions/BugActions'

const useStyles = makeStyles((theme) => ({
    input: {
        margin: theme.spacing(1, 0),
    },
}))

const AddBug = () => {
    const classes = useStyles()
    const dispatch = useDispatch()

    const onSubmit = (values) => {
        dispatch(addBug(values.description))
    }

    const formik = useFormik({
        initialValues: {
            description: '',
        },
        onSubmit,
    })

    return (
        <form onSubmit={formik.handleSubmit}>
            <TextField
                className={classes.input}
                id="description"
                variant="outlined"
                placeholder="Bug Description"
                onChange={formik.handleChange}
                fullWidth
            />
            <Button type="submit" variant="contained" color="primary">
                Create Bug
            </Button>
        </form>
    )
}

export default AddBug
