import {
    IconButton,
    List,
    ListItem,
    ListItemText,
    ListItemSecondaryAction,
} from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'
import DeleteIcon from '@material-ui/icons/Delete'
import CheckCircleOutlinedIcon from '@material-ui/icons/CheckCircleOutlined'
import CheckCircleRoundedIcon from '@material-ui/icons/CheckCircleRounded'

const useStyles = makeStyles((theme) => ({
    item: {
        borderLeft: '5px solid #fff',
        '&:hover': {
            borderLeft: '5px solid #09f',
        },
    },
}))

const BugList = ({ bugs, onResolve, onDelete }) => {
    return (
        <List>
            {bugs.map((bug) => (
                <MyItem
                    key={bug.id}
                    bug={bug}
                    onResolve={onResolve}
                    onDelete={onDelete}
                />
            ))}
        </List>
    )
}

const MyItem = ({ bug, onResolve, onDelete }) => {
    const classes = useStyles()

    return (
        <ListItem className={classes.item}>
            <ListItemText primary={bug.id + '. ' + bug.description} />
            <ListItemSecondaryAction>
                <IconButton
                    onClick={() => {
                        onResolve(bug.id)
                    }}
                    color="primary"
                >
                    {bug.resolved ? (
                        <CheckCircleRoundedIcon />
                    ) : (
                        <CheckCircleOutlinedIcon />
                    )}
                </IconButton>
                <IconButton
                    onClick={() => {
                        onDelete(bug.id)
                    }}
                    color="secondary"
                >
                    <DeleteIcon />
                </IconButton>
            </ListItemSecondaryAction>
        </ListItem>
    )
}

export default BugList
