import { ADD_BUG, REMOVE_BUG, RESOLVE_BUG } from '../actions/BugActions'

const initialState = []

let lastID = 0

export default (state = initialState, { type, payload }) => {
    switch (type) {
        case ADD_BUG:
            return [
                ...state,
                {
                    id: ++lastID,
                    description: payload.description,
                    resolved: false,
                },
            ]

        case REMOVE_BUG:
            return state.filter((bug) => bug.id !== payload.id)

        case RESOLVE_BUG:
            return state.map((bug) =>
                bug.id === payload.id
                    ? { ...bug, resolved: !bug.resolved }
                    : bug
            )

        default:
            return state
    }
}
