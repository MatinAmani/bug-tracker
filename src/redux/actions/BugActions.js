export const ADD_BUG = 'ADD_BUG'
export const addBug = (description) => ({
    type: ADD_BUG,
    payload: { description },
})

export const REMOVE_BUG = 'REMOVE_BUG'
export const removeBug = (id) => ({
    type: REMOVE_BUG,
    payload: { id },
})

export const RESOLVE_BUG = 'RESOLVE_BUG'
export const resolveBug = (id) => ({
    type: RESOLVE_BUG,
    payload: { id },
})
